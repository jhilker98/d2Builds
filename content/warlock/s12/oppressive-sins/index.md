+++
title = "Oppressive Sins"
author = ["jhilker"]
draft = false
+++

**PvE or PvP?:** PvE.

**Subclass:** Voidwalker, any branch.


## Gear {#gear}

-   **Exotic Armor**: Nezerac's Sin.


### Weapons {#weapons}

-   **Primary:** Witherhoard
-   **Energy:** [Gnawing Hunger](https://d2gunsmith.com/w/821154603?s=0,1087426260,0,0,2697220197,0) (though really any of the column 1 perks work)
-   **Heavy:** Any void weapon


### Ideal Mods {#ideal-mods}

-   Ashes to Assets
-   Energy Converter
-   Firepower